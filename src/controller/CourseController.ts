import { getRepository, getConnection } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { ACTIVE_STATUS } from "../helper/Constants";
import { SCHEDULE_STATUS } from "../helper/Constants";
import { Course } from "../entity/Course";

export class CourseController {
  private courseRepository = getRepository(Course);

  async all(request: Request, response: Response, next: NextFunction) {
    try {
      let courseList = await this.courseRepository.find();
      return {
        _isSuccess: true,
        message: "success",
        data: courseList,
        statusCode: 200,
      };
    } catch (error) {
      return { _isSuccess: false, message: "error", data: [], statusCode: 404 };
    }
  }

  async one(request: Request, response: Response, next: NextFunction) {
    try {
      let course = await this.courseRepository.findOne(request.params.id);
      return {
        _isSuccess: true,
        message: "success",
        data: course,
        statusCode: 200,
      };
    } catch (error) {
      return { _isSuccess: false, message: "error", data: [], statusCode: 404 };
    }
  }

  async save(request: Request, response: Response, next: NextFunction) {
    try {
      let courseObj = {
        courseNmae: request.body.courseNmae,
        courseCode: request.body.courseCode,
        category: request.body.category,
        courseDescription: request.body.courseDescription,
        language: request.body.language,
        courseDuration: request.body.courseDuration,
        byUserId: request.body.byUserId,
        status: ACTIVE_STATUS,
        createdAt: new Date(),
        updatedAt: new Date(),
        fromDate:null,
        toDate:null
      };
      let employee = await this.courseRepository.save(courseObj);
      return {
        _isSuccess: true,
        message: "Course has been added.",
        data: employee,
        statusCode: 200,
      };
    } catch (error) {
      return { _isSuccess: false, message: "error", data: [], statusCode: 404 };
    }
  }

  async remove(request: Request, response: Response, next: NextFunction) {
    try {
      let courseToRemove = await this.courseRepository.findOne(request.params.id);
      let deleteCourse = await this.courseRepository.remove(courseToRemove);
      return {
        _isSuccess: true,
        message: "Course has been removed.",
        data: deleteCourse,
        statusCode: 200,
      };
    } catch (error) {
      return { _isSuccess: false, message: "error", data: {}, statusCode: 404 };
    }
  }

  async courseUpdate(request: Request, response: Response, next: NextFunction) {
    try {
      let courseObj = {
        courseNmae: request.body.courseNmae,
        courseCode: request.body.courseCode,
        category: request.body.category,
        courseDescription: request.body.courseDescription,
        language: request.body.language,
        courseDuration: request.body.courseDuration,
        byUserId: request.body.byUserId,
      };
      this.courseRepository.update({ courseCode: request.body.courseCode }, courseObj);
      let updatedCourse = await this.courseRepository.findOne(request.params.id);
      return {
        _isSuccess: true,
        message: "Course has been updated.",
        data: updatedCourse,
        statusCode: 200,
      };
    } catch (err) {
      return err;
    }
  }

  async schduleCourseUpdate(request: Request, response: Response, next: NextFunction) {
    try {
      console.log("===========req===============>",request.body)
      let courseObj = {
        courseNmae: request.body.courseNmae,
        courseCode: request.body.courseCode,
        category: request.body.category,
        courseDescription: request.body.courseDescription,
        language: request.body.language,
        courseDuration: request.body.courseDuration,
        byUserId: request.body.byUserId,
        status: SCHEDULE_STATUS,
        fromDate:request.body.fromDate,
        toDate:request.body.toDate
      };
      this.courseRepository.update({ courseCode: request.body.courseCode }, courseObj);
      let updatedCourse = await this.courseRepository.findOne(request.params.id);
      return {
        _isSuccess: true,
        message: "Course has been updated.",
        data: updatedCourse,
        statusCode: 200,
      };
    } catch (err) {
      return err;
    }
  }

}
