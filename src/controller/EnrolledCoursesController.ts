import { getRepository } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { ENROLL_STATUS } from "../helper/Constants";
import { EnrolledCourses } from "../entity/EnrolledCourses";

export class EnrolledCoursesController {
  private userMappingRepository = getRepository(EnrolledCourses);

  async all(request: Request, response: Response, next: NextFunction) {
    try {
      let userMappingList = await this.userMappingRepository.find();
      console.log("============userMappingList================>",userMappingList)
      return {
        _isSuccess: true,
        message: "success",
        data: userMappingList,
        statusCode: 200,
      };
    } catch (error) {
      return { _isSuccess: false, message: "error", data: [], statusCode: 404 };
    }
  }

  async one(request: Request, response: Response, next: NextFunction) {
   
    if(request.params.flag === "U"){
      console.log("=============request.params)=== if==================>",request.params)
    try {
      let loginUserMappingList = await this.userMappingRepository.find({userId:request.params.id});
      return {
        _isSuccess: true,
        message: "success",
        data: loginUserMappingList,
        statusCode: 200,
      };
    } catch (error) {
      return { _isSuccess: false, message: "error", data: [], statusCode: 404 };
    }
  }else if(request.params.flag === "C"){
    console.log("=============request.params)==== elase=================>",request.params)
    try {
      let loginUserMappingList = await this.userMappingRepository.find({courseId:request.params.id});
      return {
        _isSuccess: true,
        message: "success",
        data: loginUserMappingList,
        statusCode: 200,
      };
    } catch (error) {
      return { _isSuccess: false, message: "error", data: [], statusCode: 404 };
    }
  }
  }

   async save(request: Request, response: Response, next: NextFunction) {
    try {
      let userMappingObj = {
        courseId: request.body.courseId,
        userId: request.body.userId,
        status: ENROLL_STATUS,
        createdAt: new Date(),
        updatedAt: new Date(),
      };
      let employee = await this.userMappingRepository.save(userMappingObj);
      return {
        _isSuccess: true,
        message: "Your has beem mapped to this course.",
        data: employee,
        statusCode: 200,
      };
    } catch (error) {
      return { _isSuccess: false, message: "error", data: [], statusCode: 404 };
    }
  }

  async remove(request: Request, response: Response, next: NextFunction) {
    try {
      let userMappingToRemove = await this.userMappingRepository.findOne(request.params.id);
      let deleteUserMapping = await this.userMappingRepository.remove(userMappingToRemove);
      return {
        _isSuccess: true,
        message: "You course mapping has been removed.",
        data: deleteUserMapping,
        statusCode: 200,
      };
    } catch (error) {
      return { _isSuccess: false, message: "error", data: {}, statusCode: 404 };
    }
  }

}
