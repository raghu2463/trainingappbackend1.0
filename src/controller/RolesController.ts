import { getRepository, getConnection } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { ACTIVE_STATUS } from "../helper/Constants";
import { Roles } from "../entity/Roles";

export class RolesController {
  private rolesRepository = getRepository(Roles);

  async all(request: Request, response: Response, next: NextFunction) {
    try {
      let role = await this.rolesRepository.find();
      return {
        _isSuccess: true,
        message: "success",
        data: role,
        statusCode: 200,
      };
    } catch (error) {
      return { _isSuccess: false, message: "error", data: [], statusCode: 404 };
    }
  }

  async one(request: Request, response: Response, next: NextFunction) {
    console.log("=========test================>",request.params.id)
    try {
      let role = await this.rolesRepository.findOne(request.params.id);
      return {
        _isSuccess: true,
        message: "success",
        data: role,
        statusCode: 200,
      };
    } catch (error) {
      return { _isSuccess: false, message: "error", data: [], statusCode: 404 };
    }
  }

}
