import { getRepository, getConnection } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { User } from "../entity/User";
import { Roles } from "../entity/Roles";
import { PRIVATE_KEY } from "../helper/Constants";
import { ACTIVE_STATUS } from "../helper/Constants";
import { USER_ROLE } from "../helper/Constants";
var jwt = require("jsonwebtoken");

export class UserController {
  private userRepository = getRepository(User);
  private roleRepository = getRepository(Roles);


  async userLogin(request: Request, response: Response, next: NextFunction) {
    if(request.body.email === ""){
      return {
        _isSuccess: false,
        message: "Email empty vlaue is not accepted",
        data: [],
        statusCode: 201,
      };
    } else if(request.body.password === ""){
      return {
        _isSuccess: false,
        message: "Password empty vlaue is not accepted",
        data: [],
        statusCode: 202,
      };
    }else{
      let checkExistUser = await this.userRepository.findOne({email:request.body.email}); 
      let userRole = await this.roleRepository.findOne(checkExistUser.roleId); 
      console.log("=========checkExistUser===========>",checkExistUser.roleId)
      console.log("=========userRole===========>",userRole)

      if(checkExistUser === undefined){       
        return {
          _isSuccess: false,
          message: "User does not exist.",
          data: [],
          statusCode: 404,
        };
        
      }else{
        let decodedPassword = jwt.verify(checkExistUser.password, PRIVATE_KEY); 
        console.log("=========decodedPassword.foo===========>",decodedPassword.foo)
        console.log("=========request.body.password===========>",request.body.password) 
        if(request.body.password === decodedPassword.foo){
          let token = jwt.sign({foo:checkExistUser}, PRIVATE_KEY);
          return {
            _isSuccess: true,
            message: "Login success.",
            token: token,
            data: checkExistUser,
            statusCode: 200,
          };
        }else{
          return {
            _isSuccess: false,
            message: "Password dose not match",
            data: [],
            statusCode: 203,
          };
        }
      }
    }
    
  }

  async all(request: Request, response: Response, next: NextFunction) {
    try {
      let userList = await this.userRepository.find();
      return {
        _isSuccess: true,
        message: "success",
        data: userList,
        statusCode: 200,
      };
    } catch (error) {
      return { _isSuccess: false, message: "error", data: [], statusCode: 404 };
    }
  }

  async one(request: Request, response: Response, next: NextFunction) {
    try {
      let user = await this.userRepository.findOne(request.params.id);
      return {
        _isSuccess: true,
        message: "success",
        data: user,
        statusCode: 200,
      };
    } catch (error) {
      return { _isSuccess: false, message: "error", data: [], statusCode: 404 };
    }
  }

  async userRegister(request: Request, response: Response, next: NextFunction) {
    console.log("============request.body==================>",request.body)
    let existUser = await this.userRepository.findOne({email:request.body.email});
    console.log("============existUser==================>",existUser)
    try {
      if(existUser === undefined){
        console.log("============if==================>")
        let userObj = {
          firstName: request.body.firstName,
          lastName: request.body.lastName,
          email: request.body.email,
          password: jwt.sign({ foo: request.body.password }, PRIVATE_KEY),
          phoneNumber: request.body.phoneNumber,
          headLine: request.body.headLine,
          userImage: request.body.userImage,
          linkedIn: request.body.linkedIn,
          github: request.body.github,
          youTube: request.body.youTube,
          roleId: USER_ROLE,
          status: ACTIVE_STATUS,
          createdAt: new Date(),
          updatedAt: new Date(),
          roleCode:'U'
        };
        let user = await this.userRepository.save(userObj);
        return {
          _isSuccess: true,
          message: "User has been registered.",
          data: user,
          statusCode: 200,
        };
      }else{
        console.log("============else==================>")
        return {
          _isSuccess: false,
          message: "This user has been already existed.",
          data: [],
          statusCode: 304,
        };
      }      
    } catch (error) {
      return "error";
    }
  }

  async save(request: Request, response: Response, next: NextFunction) {
    try {
      let existUser = await this.userRepository.findOne(request.body.email);
      let userObj = {
        firstName: request.body.firstName,
        lastName: request.body.lastName,
        email: request.body.email,
        password: jwt.sign({ foo: request.body.email }, PRIVATE_KEY),
        phoneNumber: request.body.phoneNumber,
        headLine: request.body.headLine,
        userImage: request.body.userImage,
        linkedIn: request.body.linkedIn,
        github: request.body.github,
        youTube: request.body.youTube,
        roleId: USER_ROLE,
        status: ACTIVE_STATUS,
        createdAt: new Date(),
        updatedAt: new Date(),
        roleCode:'U'
      };
      let employee = await this.userRepository.save(userObj);
      return {
        _isSuccess: true,
        message: "User has been added.",
        data: employee,
        statusCode: 200,
      };
    } catch (error) {
      return { _isSuccess: false, message: "error", data: [], statusCode: 404 };
    }
  }

  async remove(request: Request, response: Response, next: NextFunction) {
    try {
      let userToRemove = await this.userRepository.findOne(request.params.id);
      let deleteEmployee = await this.userRepository.remove(userToRemove);
      return {
        _isSuccess: true,
        message: "User has been removed.",
        data: deleteEmployee,
        statusCode: 200,
      };
    } catch (error) {
      return { _isSuccess: false, message: "error", data: {}, statusCode: 404 };
    }
  }

  async userUpdate(request: Request, response: Response, next: NextFunction) {
    try {
      let userObj = {
        firstName: request.body.firstName,
        lastName: request.body.lastName,
        email: request.body.email,
        phoneNumber: request.body.phoneNumber,
        headLine: request.body.headLine,
        linkedIn: request.body.linkedIn,
        github: request.body.github,
        youTube: request.body.youTube,       
      };
      this.userRepository.update({ email: request.body.email }, userObj);
      let updatedUser = await this.userRepository.findOne(request.params.id);
      return {
        _isSuccess: true,
        message: "User has been updated.",
        data: updatedUser,
        statusCode: 200,
      };
    } catch (err) {
      return err;
    }
  }
}
