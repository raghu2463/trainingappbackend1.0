import {Entity, ObjectIdColumn, ObjectID, Column} from "typeorm";

@Entity()
export class Course {

    @ObjectIdColumn()
    id: ObjectID;

    @Column()
    courseNmae : string;

    @Column()
    courseCode: string;   

    @Column()
    category: string;   

    @Column()
    courseDescription : string;

    @Column()
    language : string;

    @Column()
    courseDuration : string;

    @Column()
    byUserId : string;

    @Column()
    status : string;

    @Column()
    createdAt: Date;

    @Column()
    updatedAt: Date;

    @Column()
    fromDate: Date;

    @Column()
    toDate: Date;


}
