import {Entity, ObjectIdColumn, ObjectID, Column} from "typeorm";

@Entity()
export class EnrolledCourses {

    @ObjectIdColumn()
    id: ObjectID;

    @Column()
    courseId : string;

    @Column()
    userId: string;   

    @Column()
    status: string;

    @Column()
    createdAt: Date;

    @Column()
    updatedAt: Date;

}
