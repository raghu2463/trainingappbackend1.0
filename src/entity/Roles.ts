import {Entity, ObjectIdColumn, ObjectID, Column} from "typeorm";

@Entity()
export class Roles {

    @ObjectIdColumn()
    id: ObjectID;

    @Column()
    roleName: string;

    @Column()
    roleCode: string;   

    @Column()
    status: string;

    @Column()
    createdAt: Date;

    @Column()
    updatedAt: Date;

}
