import {Entity, ObjectIdColumn, ObjectID, Column} from "typeorm";

@Entity()
export class User {

    @ObjectIdColumn()
    id: ObjectID;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column()
    phoneNumber: string;

    @Column()
    headLine: string; 

    @Column()
    userImage: string;

    @Column()
    linkedIn: string;

    @Column()
    github: string;

    @Column()
    youTube: string;

    @Column()
    roleId: string;

    @Column()
    status: string;

    @Column()
    createdAt: Date;

    @Column()
    updatedAt: Date;

    @Column()
    roleCode: string;

}
