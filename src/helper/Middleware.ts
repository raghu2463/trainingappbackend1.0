import { getRepository } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { User } from "../entity/User";
import { PRIVATE_KEY } from "./constants";
const jwt = require("jsonwebtoken");

export class Middleware {
  private userRepository = getRepository(User);
  async Auth(req: Request, res: Response, next: NextFunction) {
    try {
      let userToken = req.headers.authorization;
      let decodedTocken = jwt.verify(userToken, PRIVATE_KEY);
      let existUser = await this.userRepository.findOne(decodedTocken.id);
      if (existUser) {
        return {
          status: "success",
          message: "Authorized Sccess",
          statusCode: 200,
        };       
      } else {
        return {
            status: "failed",
            message: "UnAuthorized Access",
            statusCode: 401,
          };    
      }
    } catch (error) {
        return res.status(401).json({status:'failed',message: 'UnAuthorized Access',statusCode:401});
    }

    // console.log("Authoriztion Header ==> "+req.headers.authorization);
    // let bearerHeader = req.headers.authorization;

    // let fetchToken = bearerHeader.split(' ');
    // console.log("FetchToken =>"+JSON.stringify(fetchToken));

    // let decipheredData = await decryptData(fetchToken[1]);
    // console.log("Decipher ===> " + decipheredData)
    // let fetchUserObject = JWT.verify(decipheredData, PRIVATE_KEY);
    // console.log("Middleware Step 5")

    // let  userRepository = getRepository(Employee);

    // let user = await userRepository.findOne((fetchUserObject.id));

    // console.log("Middleware Step 6 => ")
    // if (user !== null) {
    //    console.log("Middleware Step 7 ===> "+JSON.stringify(user))
    //     return res.json({ status: 'success', user: user})
    // } else {
    //  console.log("Middleware Step 8")
    //     return res.json({ status: 'Failed',message: "UnAuthorized Access" })
    // }
  }
}
