import "reflect-metadata";
import { createConnection } from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import { Request, Response } from "express";
import { UserRouter } from "./router/userRoutes";
import { CourseRoutes } from "./router/courseRoutes";
import { UserMappingRoutes } from "./router/userMappingRoutes";
import { RolesRoutes } from "./router/rolesRoutes";
import { User } from "./entity/User";
import { Roles } from "./entity/Roles";
import { PRIVATE_KEY } from "./helper/Constants";
import { ACTIVE_STATUS } from "./helper/Constants";
import { USER_ROLE } from "./helper/Constants";
var jwt = require("jsonwebtoken");

createConnection()
  .then(async (connection) => {
    // create express app
    const app = express();
    app.use(bodyParser.json());

    // Course routes
    UserRouter.forEach(route => {
      (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
          if(route.middleware !== undefined){
              const middle = (new (route.middleware as any))["Auth"] (req, res, next);
              if (middle instanceof Promise) {
                  middle.then(result => { 
                      if(result.status === 'success'){
                          const result = (new (route.controller as any))[route.action] (req, res, next);
                          if (result instanceof Promise) {
                              result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);
                          }else if (result !== null && result !== undefined) {
                              res.json(result);
                          }
                      }else{
                          res.json(result);
                      }
                  });
              } else if (middle !== null && middle !== undefined) {
                  res.json(middle);
              }
          }else{
              const result = (new (route.controller as any))[route.action] (req, res, next);
              if (result instanceof Promise) {
                  result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);
              }else if (result !== null && result !== undefined) {
                  res.json(result);
              }
          }
      });
  });


  // Course routes
  CourseRoutes.forEach(route => {
    (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
        if(route.middleware !== undefined){
            const middle = (new (route.middleware as any))["Auth"] (req, res, next);
            if (middle instanceof Promise) {
                middle.then(result => { 
                    if(result.status === 'success'){
                        const result = (new (route.controller as any))[route.action] (req, res, next);
                        if (result instanceof Promise) {
                            result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);
                        }else if (result !== null && result !== undefined) {
                            res.json(result);
                        }
                    }else{
                        res.json(result);
                    }
                });
            } else if (middle !== null && middle !== undefined) {
                res.json(middle);
            }
        }else{
            const result = (new (route.controller as any))[route.action] (req, res, next);
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);
            }else if (result !== null && result !== undefined) {
                res.json(result);
            }
        }
    });
});

// User mapping routes
UserMappingRoutes.forEach(route => {
  (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
      if(route.middleware !== undefined){
          const middle = (new (route.middleware as any))["Auth"] (req, res, next);
          if (middle instanceof Promise) {
              middle.then(result => { 
                  if(result.status === 'success'){
                      const result = (new (route.controller as any))[route.action] (req, res, next);
                      if (result instanceof Promise) {
                          result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);
                      }else if (result !== null && result !== undefined) {
                          res.json(result);
                      }
                  }else{
                      res.json(result);
                  }
              });
          } else if (middle !== null && middle !== undefined) {
              res.json(middle);
          }
      }else{
          const result = (new (route.controller as any))[route.action] (req, res, next);
          if (result instanceof Promise) {
              result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);
          }else if (result !== null && result !== undefined) {
              res.json(result);
          }
      }
  });
});

// Roles routes
RolesRoutes.forEach(route => {
    (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
        if(route.middleware !== undefined){
            const middle = (new (route.middleware as any))["Auth"] (req, res, next);
            if (middle instanceof Promise) {
                middle.then(result => { 
                    if(result.status === 'success'){
                        const result = (new (route.controller as any))[route.action] (req, res, next);
                        if (result instanceof Promise) {
                            result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);
                        }else if (result !== null && result !== undefined) {
                            res.json(result);
                        }
                    }else{
                        res.json(result);
                    }
                });
            } else if (middle !== null && middle !== undefined) {
                res.json(middle);
            }
        }else{
            const result = (new (route.controller as any))[route.action] (req, res, next);
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);
            }else if (result !== null && result !== undefined) {
                res.json(result);
            }
        }
    });
  });

    // setup express app here
    // ...

    // start express server
    app.listen(3000);

    // insert new users for test
    // await connection.manager.save(
    //   connection.manager.create(User, {
    //     firstName: "Vishwa",
    //     lastName: "Kathir",
    //     email: "vishwa@gmail.com",
    //     password: jwt.sign({ foo: "password" }, PRIVATE_KEY),
    //     phoneNumber:"9443351675",
    //     headLine :"Sample Text",
    //     userImage :"",
    //     linkedIn:"",
    //     github :"",
    //     youTube:"", 
    //     roleId: USER_ROLE,
    //     status: ACTIVE_STATUS,
    //     createdAt:new Date(),
    //     updatedAt:new Date(),
    //     roleCode: 'A',
    //   })
    // );

     // insert new users for test
    //  await connection.manager.save(
    //     connection.manager.create(User, {
    //       firstName: "Vishwanathan",
    //       lastName: "Kkathir",
    //       email: "nathan@gmail.com",
    //       password: jwt.sign({ foo: "password" }, PRIVATE_KEY),
    //       phoneNumber:"9443351675",
    //       headLine :"Sample Text",
    //       userImage :"",
    //       linkedIn:"",
    //       github :"",
    //       youTube:"", 
    //       roleId: USER_ROLE,
    //       status: ACTIVE_STATUS,
    //       createdAt:new Date(),
    //       updatedAt:new Date(),
    //       roleCode: 'U',
    //     })
    //   );

    // insert new roles for test
    // await connection.manager.save(
    //   connection.manager.create(Roles, {
    //     roleName: "Admin",
    //     roleCode: "A",
    //     status: ACTIVE_STATUS,
    //     createdAt:new Date(),
    //     updatedAt:new Date()
    //   })
    // );

    // // insert new roles for test
    // await connection.manager.save(
    //   connection.manager.create(Roles, {
    //     roleName: "User",
    //     roleCode: "U",
    //     status: ACTIVE_STATUS,
    //     createdAt:new Date(),
    //     updatedAt:new Date()
    //   })
    // );
 

    console.log(
      "Express server has started on port 3000. Open http://localhost:3000/users to see results"
    );
  })
  .catch((error) => console.log(error));
