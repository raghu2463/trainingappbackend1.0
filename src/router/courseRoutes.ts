import { CourseController } from "../controller/CourseController";
import { Middleware } from "../helper/Middleware";

export const CourseRoutes = [
  {
    method: "get",
    route: "/api/course",
    middleware:Middleware,
    controller: CourseController,
    action: "all",
  },
  {
    method: "get",
    route: "/api/course/:id",
    middleware:Middleware,
    controller: CourseController,
    action: "one",
  },
  {
    method: "post",
    route: "/api/course",
    middleware:Middleware,
    controller: CourseController,
    action: "save",
  },
  {
    method: "delete",
    route: "/api/course/:id",
    middleware:Middleware,
    controller: CourseController,
    action: "remove",
  },
  {
    method: "put",
    route: "/api/course/:id",
    middleware:Middleware,
    controller: CourseController,
    action: "courseUpdate",
  },
  {
    method: "put",
    route: "/api/schduleCourse/:id",
    middleware:Middleware,
    controller: CourseController,
    action: "schduleCourseUpdate",
  },
  
];
