import { Middleware } from "../helper/Middleware";
import { RolesController } from "../controller/RolesController";

export const RolesRoutes = [
  {
    method: "get",
    route: "/api/roles",
    middleware:Middleware,
    controller: RolesController,
    action: "all",
  },
  {
    method: "get",
    route: "/api/roles/:id",
    middleware:Middleware,
    controller: RolesController,
    action: "one"
  }
];
