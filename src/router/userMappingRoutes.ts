import { EnrolledCoursesController } from "../controller/EnrolledCoursesController";
import { Middleware } from "../helper/Middleware";

export const UserMappingRoutes = [
  {
    method: "get",
    route: "/api/userMappingRoutes",
    middleware:Middleware,
    controller: EnrolledCoursesController,
    action: "all",
  },
  {
    method: "get",
    route: "/api/userMappingRoutes/:id/:flag",
    middleware:Middleware,
    controller: EnrolledCoursesController,
    action: "one",
  },
  {
    method: "post",
    route: "/api/userMappingRoutes",
    middleware:Middleware,
    controller: EnrolledCoursesController,
    action: "save",
  },
  {
    method: "delete",
    route: "/api/userMappingRoutes/:id",
    middleware:Middleware,
    controller: EnrolledCoursesController,
    action: "remove",
  },  
];
