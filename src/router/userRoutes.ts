import { UserController } from "../controller/UserController";
import { Middleware } from "../helper/Middleware";

export const UserRouter = [
  {
    method: "post",
    route: "/api/login",
    controller: UserController,
    action: "userLogin",
  },
  {
    method: "post",
    route: "/api/register",
    controller: UserController,
    action: "userRegister",
  },
  {
    method: "get",
    route: "/api/users",
    middleware:Middleware,
    controller: UserController,
    action: "all",
  },
  {
    method: "get",
    route: "/api/users/:id",
    middleware:Middleware,
    controller: UserController,
    action: "one",
  },
  {
    method: "post",
    route: "/api/users",
    controller: UserController,
    action: "save",
  },
  {
    method: "delete",
    route: "/api/users/:id",
    middleware:Middleware, 
    controller: UserController,
    action: "remove",
  },
  {
    method: "put",
    route: "/api/users/:id",
    middleware:Middleware,
    controller: UserController,
    action: "userUpdate",
  },
];
